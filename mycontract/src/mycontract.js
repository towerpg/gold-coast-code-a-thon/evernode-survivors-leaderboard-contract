const HotPocket = require("hotpocket-nodejs-contract");
const fs = require("fs").promises;

const mycontract = async (ctx) => {
    // Your smart contract logic.
    for (const user of ctx.users.list()) {
        console.log("User public key", user.publicKey);

        // Loop through inputs sent by each user.
        for (const input of user.inputs) {
            const buffer = await ctx.users.read(input);

            const message = buffer.toString();

            const filename = "leaderboard.txt";
            await fs.appendFile(filename, message + "\n")

            const allMessage = (await fs.readFile(filename)).toString();
            console.log("All messages:", allMessage);
            const rank1 = findHighScore(allMessage);

            await user.send(rank1);
        }
    }
}

function findHighScore(allMessages) {

    let highScoreEntry = "";
    let highScore = 0;
    let scores = [];
    console.log("All messages:", allMessages);
    // write a for loop that iterates through allMessages split by "\n"
    for (const message of allMessages.split("\n")) {
        if (message !== "") {
            console.log("Message:", message);
            // split each message by " " and store the second element in a variable
            let jsonMessage = JSON.parse(message)
            scores.push(jsonMessage)
        }
    }

    return scores;
}

const hpc = new HotPocket.Contract();
hpc.init(mycontract);